TEST INE RESTFUL DATABASE ENGINE
================================
Prueba tecnica de python django dev.


**Implementación**
------------------
El enunciado de test dice emular funciones básicas de una base de datos.

Haciendo un paralelismos entre entre las funciones básicas a implementar y las especificaciones de los verbos http 
en APIRESTful usaremos POST para crear objetos(Tabla, un registro), GET para extraer información(detalle de una tabla, 
registros), DELETE para eliminar objetos(DROP de una tabla).


# API endpoints

## CREATE TABLE

Crear una tabla con sus respectivos fields.

**URL** : `/api/table/`

**Method** : `POST`

**Auth required** : YES

**Data example**

```json
{
    "name": "movies",
    "fields": {
      "title": {
        "type": "str",
        "is_unique": true
      },
      "release_date": "datetime",
      "imdb_ranking": "float",
      "director": {
        "type": "int",
        "foreign_key": "director.id"
      }   
    } 
}
```

#### Success Response

**Code** : `201 CREATED`

#### Error Response

**Condition** : La tabla que desea crear ya existe.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "detail": "The table could not be created, a table with that name already exists."
}
```

##### Or

**Condition** : El field tiene un tipo de dato no valido.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "detail": "Incorrect data type for total field."
}
```

##### Or

**Condition** : La tabla con una foreign key relation no existe.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "detail": "foreign key table does not exist."
}
```

---

## SHOW TABLE

Listar tablas.

**URL** : `/api/table/`

**Method** : `GET`

**Auth required** : YES

#### Success Response

**Code** : `200 OK`

**Response example**

```json
[
  {
    "name": "movies"
  },
  {
    "name": "directors"
  }
]
```

---

## DROP TABLE

Eliminar una tabla existente.

**URL** : `/api/table/{NAME_TABLE}/`

**Method** : `DELETE`

**Auth required** : YES


#### Success Response

**Code** : `204 NO CONTENT`

#### Error Response

**Condition** : La tabla que desea eliminar no existe.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "detail": "The table you want to delete does not exist.."
}
```

____

## INSERT

Insertar datos a una tabla.

**URL** : `/api/table/{NAME_TABLE}/`

**Method** : `POST`

**Auth required** : YES

**Data example**

```json
{
    "title": "Avenger",
    "release_date": "2012-04-26",
    "imdb_ranking": 8.4,
    "director": {
      "id": 1
    } 
}
```

#### Success Response

**Code** : `201 CREATED`

#### Error Response

**Condition** : La tabla seleccionada no existe.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "detail": "The selected table does not exist."
}
```

##### Or

**Condition** : El field tiene un tipo de dato no valido.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "detail": "Incorrect data type for total field."
}
```

##### Or

**Condition** : La tabla con una foreign key relation no existe.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "detail": "foreign key table does not exist."
}
```

---

## SELECT

Selecciona una serie de registros de una tabla dada.

**URL** : `/api/table/{NAME_TABLE}/?title=avenger`

**Method** : `GET`

**Auth required** : YES

#### Success Response

**Code** : `20O OK`

**Response example**

```json
[
  {
    "title": "The Avengers",
    "release_date": "2012-04-26",
    "imdb_ranking": 8.4,
    "director": {
      "id": 1
    } 
  },
  {
    "title": "The Avengers 2",
    "release_date": "2012-04-26",
    "imdb_ranking": 8.4,
    "director": {
      "id": 1
    } 
  }  
]
```

#### Error Response

**Condition** : La tabla seleccionada no existe.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "detail": "The selected table does not exist."
}
```

##### Or

**Condition** : El field de filtro no existe en la tabla.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "detail": "total Field not found in table."
}
```

## DESCRIBE TABLE


Crear una tabla con sus respectivos fields.

**URL** : `/api/table/{NAME_TABLE}/describe/`

**Method** : `GET`

**Auth required** : YES

#### Success Response

**Code** : `20O OK`

**Response example**

```json
[
  {
    "column": "title",
    "type": "str",
    "nulable": false
  },
  {
    "column": "release_date",
    "type": "datetime",
    "nulable": false
  },
  {
    "column": "imdb_ranking",
    "type": "float",
    "nulable": false
  }
]
```

#### Error Response

**Condition** : La tabla seleccionada no existe.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "detail": "The selected table does not exist."
}
```


### Notas

* Las funcionalidades de una base en si son numerosas y no se si es el fin de esta prueba por lo tanto trate de 
mantener simple su funcionalidad.